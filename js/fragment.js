(function () {
  let listContainer = document.getElementById('list-container');
  let uList = document.getElementById('uList');

  let fragment = document.getElementById('fragment');
  let normal = document.getElementById('normal');

  let clone = document.getElementById('clone');
  let changeAccess = document.getElementById('changeAccess');

  let iterations = 1000;

  let li;

  let fnClearUList = ()=> {
    uList = document.getElementById('uList');
    uList.innerHTML = '';
  };

  let marksList = document.getElementById('marks');
  let setMarks = (text, param) => {
    let li = document.createElement('li');
    li.textContent = text;
    li.classList.add(param);
    marksList.appendChild(li);
  };

  let fnChangeList = (nodeList) => {
    let itemList = nodeList.childNodes;
    let itemLength = itemList.length;
    for (let j = 0; j < itemLength; j++) {
      itemList[j].innerHTML += ' ->  ' + (j);
    }
    return nodeList;
  };

  fragment.addEventListener('click', ()=> {
    fnClearUList();
    setTimeout(()=> {
      let docFragment = new DocumentFragment();
      let t0 = performance.now();
      for (let i = 0; i < iterations; i++) {
        li = document.createElement('li');
        li.textContent = 'Text ' + i;
        docFragment.appendChild(li);
      }
      uList.appendChild(docFragment);
      let t1 = performance.now();

      setMarks('Fragment time: ' + parseInt(t1 - t0) + ' ms', 'append');
    }, 1000);

  });

  normal.addEventListener('click', ()=> {
    fnClearUList();
    setTimeout(()=> {
      let t0 = performance.now();
      for (let i = 0; i < iterations; i++) {
        li = document.createElement('li');
        li.textContent = 'Text ' + i;
        uList.appendChild(li);
      }
      let t1 = performance.now();

      setMarks('Append time: ' + parseInt(t1 - t0) + ' ms', 'append');
    }, 1000);
  });

  clone.addEventListener('click', ()=> {
    let t0 = performance.now();
    let clone = uList.cloneNode(true);
    listContainer.innerHTML = '';
    clone = fnChangeList(clone);
    clone.id = 'uList';
    listContainer.appendChild(clone);
    let t1 = performance.now();
    setMarks('Clone time: ' + parseInt(t1 - t0) + ' ms', 'change');
  });

  changeAccess.addEventListener('click', ()=> {
    let t0 = performance.now();
    fnChangeList(uList);
    let t1 = performance.now();
    setMarks('Change time: ' + parseInt(t1 - t0) + ' ms', 'change');
  });
})();
